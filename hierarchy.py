import pandas as pd
from tqdm import tqdm

df = pd.read_csv("hierarchy.txt",sep="\t",header=None,names="parentId childId type".split())

ids = df.parentId.values.tolist()
ids.extend(df.childId.values.tolist())
ids = list(set(ids))

inclusion_relations_ = dict(df["childId parentId".split()].values)


inc_dict_geonames = {} 
for childId,parentId in tqdm(inclusion_relations_.items()):
    if not childId in inc_dict_geonames:
        inc_dict_geonames[childId] = [parentId]
        if parentId in inc_dict_geonames:
            inc_dict_geonames[childId].extend(inc_dict_geonames[parentId])
        else:
            B = parentId
            while 1:
                if B in inclusion_relations_:
                    inc_dict_geonames[childId].append(inclusion_relations_[B])
                    B = inclusion_relations_[B]
                else:
                    break
            inc_dict_geonames[parentId] = inc_dict_geonames[childId][1:]
            
import json
path="out_final_extended.json"
geonames2GD,wikidata2GD = {}, {}

from mytoolbox.text.size import wc_l

size_data = wc_l(path)

for line in tqdm(open(path),total=size_data):
    data = json.loads(line.strip("\n,"))
    if "geonameID" in data:
        geonames2GD[data["geonameID"]]=data["id"]
    if "wikidataID" in data:
        wikidata2GD[data["wikidataID"]]=data["id"]

output_path = "geodict_final_29_04_19.json"

output = open(output_path,'w')

name_col = {"P131":"located_in_adm_terr_ent",
            "P706":"located_in_terr_feature",
            "P47":"share_border_with"}

for line in tqdm(open(path),total=size_data):
    data = json.loads(line.strip("\n,"))
    for property_ in ["P131","P706","P47"]:
        if not property_ in data:
            continue
        data[name_col[property_]] = [wikidata2GD[id_] for id_ in data[property_] if id_ in wikidata2GD]
    if "geonameID" in data and data["geonameID"] in inc_dict_geonames:
        data["geoname_hierarchy"] = inc_dict_geonames[data["geonameID"]]
    output.write("{0}\n,".format(json.dumps(data)))